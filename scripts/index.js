const table1 = document.querySelector(".tables .table-1");
const table1_rows = document.querySelector(".table-1 .rows");

const modal_edit = document.getElementById("employee-info");
const modal_delete = document.getElementById("modal-delete");
const modal_delete_name = document.querySelector("#modal-delete h2");

let employees = [];

(function () {
  fetch("http://localhost:3000/employees")
    .then((resp) => resp.json())
    .then((data) => {
      employees = data;
      displayData();
    });
})();

async function getManagers() {
  const managers = await fetch("http://localhost:3000/managers").then((resp) =>
    resp.json()
  );
  return managers;
}

function resetRowColors() {
  const rows = table1_rows.childNodes;

  rows.forEach((row) => {
    row.style.backgroundColor = "transparent";
  });
}

async function displayData() {
  table1_rows.innerHTML = "";

  const managers = await getManagers();

  employees.forEach((employee) => {
    const row = document.createElement("div");
    row.classList.add("row");
    row.id = employee["employeeNumber"];

    row.addEventListener("click", async (e) => {
      // Checking if it is not buttons that were pressed
      if (e.target.innerHTML !== "Edit" && e.target.innerHTML !== "Delete") {
        const office_code = parseInt(employee["officeCode"]);

        const office = await fetch(
          "http://localhost:3000/office/" + office_code
        ).then((resp) => resp.json());

        resetRowColors();
        row.style.backgroundColor = "rgb(219, 255, 224)";

        // Filling in table 2 with office info
        document.getElementById("officeCode").innerHTML = office["officeCode"];
        document.getElementById("city").innerHTML = office["city"];
        document.getElementById("phone").innerHTML = office["phone"];
        document.getElementById("addressLine1").innerHTML =
          office["addressLine1"];
        document.getElementById("addressLine2").innerHTML =
          office["addressLine2"];
        document.getElementById("state").innerHTML = office["state"];
        document.getElementById("country").innerHTML = office["country"];
        document.getElementById("postalCode").innerHTML = office["postalCode"];
        document.getElementById("territory").innerHTML = office["territory"];
      }
    });

    // get the index of the employee manager
    const manager_index = managers
      .map((mng) => mng["reportsTo"])
      .indexOf(employee["reportsTo"]);

    const manager = managers[manager_index];

    row.innerHTML = `<div class="col name-cell">
        <img src="./images/prof-img.png" alt="profile image" />
        <div class="person-info">
            <p class="name">${
              employee["firstName"] + " " + employee["lastName"]
            }</p>
            <p class="email">${employee["email"]}</p>
        </div>
    </div>
    <div class="col title-cell">
        <p>${employee["jobTitle"]}</p>
    </div>
    <div class="col responds-cell">${
      manager["firstName"] + " " + manager["lastName"]
    }</div>
    <div class="col button-cell">
        <a href="#" class="edit" onclick="editEmployee('${
          employee["email"]
        }')">Edit</a>
        <a href="#" class="delete" onclick="deleteWarning('${
          employee["email"]
        }')">Delete</a>
    </div>`;

    table1_rows.appendChild(row);
  });
}

async function editEmployee(employee_email) {
  employee_index = getEmpIndex(employee_email);

  let { firstName, lastName, email, jobTitle, reportsTo } =
    employees[employee_index];

  modal_edit.style.display = "inline-block";

  document.querySelector("#employee-info #first_name").value = firstName;
  document.querySelector("#employee-info #last_name").value = lastName;
  document.querySelector("#employee-info #email").value = email;
  document.querySelector("#employee-info #title").value = jobTitle;

  const managers = await getManagers();

  const select_element = document.querySelector("#employee-info #responds-to");
  select_element.innerHTML = "";

  managers.forEach((manager) => {
    let option = document.createElement("option");
    option.value = manager["reportsTo"];
    option.innerHTML =
      manager["firstName"] +
      " " +
      manager["lastName"] +
      " (" +
      manager["reportsTo"] +
      ")";
    if (manager["reportsTo"] === reportsTo) {
      option.selected = true;
    }
    select_element.appendChild(option);
  });
}

let delete_emp_index = -1;

function deleteWarning(employee_email) {
  modal_delete.style.display = "inline-block";

  delete_emp_index = getEmpIndex(employee_email);
  modal_delete_name.innerHTML =
    employees[delete_emp_index]["firstName"] +
    " " +
    employees[delete_emp_index]["lastName"];
}

function deleteEmployee() {
  closeModal("delete");

  employees.splice(delete_emp_index, 1);

  displayData();
}

function changeEmployee(e) {
  e.preventDefault();

  let url = "http://localhost:3000/employees";
  let met = "POST";

  const email = document.querySelector("#employee-info #email").value;
  const exists = getEmpIndex(email);

  const selected_val = document.querySelector(
    "#employee-info #responds-to"
  ).value;

  // if -1 then add new employee
  if (exists === -1 && email !== "") {
    const employee = {};
    employee["firstName"] = document.querySelector(
      "#employee-info #first_name"
    ).value;
    employee["lastName"] = document.querySelector(
      "#employee-info #last_name"
    ).value;
    employee["jobTitle"] = document.querySelector(
      "#employee-info #title"
    ).value;
    employee["email"] = email;
    employee["reportsTo"] = selected_val;
    employee["officeCode"] = 6;
  } else {
    url += "/" + selected_val;
    met = "PUT";
  }

  fetch(url, {
    method: met,

    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((resp) => resp.json())
    .then((data) => {
      employees = data;
      closeModal("edit");
      displayData();
    });
}

function closeModal(modal) {
  if (modal === "edit") {
    modal_edit.style.display = "none";
  } else if (modal === "delete") {
    modal_delete.style.display = "none";
  }
}

function getEmpIndex(emp_email) {
  let index = employees.map((employee) => employee["email"]).indexOf(emp_email);
  return index;
}

async function addNewEmployee() {
  modal_edit.style.display = "inline-block";

  const managers = await getManagers();

  const select_element = document.querySelector("#employee-info #responds-to");
  select_element.innerHTML = "";

  managers.forEach((manager) => {
    let option = document.createElement("option");
    option.value = manager["reportsTo"];
    option.innerHTML =
      manager["firstName"] +
      " " +
      manager["lastName"] +
      " (" +
      manager["reportsTo"] +
      ")";
    select_element.appendChild(option);
  });
}
